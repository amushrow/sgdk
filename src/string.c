#include "config.h"
#include "types.h"

#include "string.h"

#include "tab_cnv.h"
#include "maths.h"
#include "memory.h"


#define P01 10
#define P02 100
#define P03 1000
#define P04 10000
#define P05 100000
#define P06 1000000
#define P07 10000000
#define P08 100000000
#define P09 1000000000
#define P10 10000000000

#if (ENABLE_NEWLIB == 0)
static const char uppercase_hexchars[] = "0123456789ABCDEF";
static const char lowercase_hexchars[] = "0123456789abcdef";
#endif  // ENABLE_NEWLIB
static const char digits[] =
    "0001020304050607080910111213141516171819"
    "2021222324252627282930313233343536373839"
    "4041424344454647484950515253545556575859"
    "6061626364656667686970717273747576777879"
    "8081828384858687888990919293949596979899";

// FORWARD
static u16 digits10(const u16 v);
static u16 uint16ToStr(u16 value, char *str, u16 minsize);
#if (ENABLE_NEWLIB == 0)
static u16 skip_atoi(const char **s);
static u16 vsprintf(char *buf, const char *fmt, va_list args);
#endif  // ENABLE_NEWLIB


#if (ENABLE_NEWLIB == 0)
u16 strlen(const char *str)
{
    const char *src;

    src = str;
    while (*src++);

    return (src - str) - 1;
}

u16 strnlen(const char *str, u16 maxlen)
{
    const char *src;

    for (src = str; maxlen-- && *src != '\0'; ++src)
        /* nothing */;

    return src - str;
}

s16 strcmp(const char *str1, const char *str2)
{
    const u8 *p1 = (const u8*) str1;
    const u8 *p2 = (const u8*) str2;
    u8 c1, c2;

    do
    {
        c1 = *p1++;
        c2 = *p2++;
    }
    while (c1 && (c1 == c2));

    return c1 - c2;
}

char* strclr(char *str)
{
    str[0] = 0;

    return str;
}

char* strcpy(char *to, const char *from)
{
    const char *src;
    char *dst;

    src = from;
    dst = to;
    while ((*dst++ = *src++));

    return to;
}

char* strncpy(char *to, const char *from, u16 len)
{
    const char *src;
    char *dst;
    u16 i;

    src = from;
    dst = to;
    i = 0;
    while ((i++ < len) && (*dst++ = *src++));

    // end string by null character
    if (i > len) *dst = 0;

    return to;
}

//char* strcpy2(char *to, const char *from)
//{
//  char *pto = to;
//  unsigned int n = 0xFFFF;
//
//  asm volatile ("1:\n"
//       "\tmove.b (%0)+,(%1)+\n"
//       "\tbne.b 1b\n" :
//      "=a" (from), "=a" (pto), "=d" (n) :
//       "0" (from),  "1" (pto), "2" (n) :
//       "cc", "memory");
//  return to;
//}

char* strcat(char *to, const char *from)
{
    const char *src;
    char *dst;

    src = from;
    dst = to;
    while (*dst++);

    --dst;
    while ((*dst++ = *src++));

    return to;
}

char *strreplacechar(char *str, char oldc, char newc)
{
    char *s;

    s =  str;
    while(*s)
    {
        if (*s == oldc)
            *s = newc;
        s++;
    }

    return s;
}

const char* strrchr(const char* str, int character)
{
	const char* lastLoc = NULL;
	while (*str != 0)
	{
		if (*str == character)
			lastLoc = str;

		++str;
	}
	return lastLoc;
}

const char* strchr(const char* str, int character)
{
	while (*str != 0)
	{
		if (*str == character)
			return str;
		++str;
	}

	return NULL;
}
#endif  // ENABLE_NEWLIB


u16 intToStr(s32 value, char *str, u16 minsize)
{
    if (value < -500000000)
    {
        strcpy(str, "<-500000000");
        return 10;
    }

    if (value < 0)
    {
        *str = '-';
        return uintToStr(-value, str + 1, minsize);
    }
    else return uintToStr(value, str, minsize);
}

u16 uintToStr(u32 value, char *str, u16 minsize)
{
    if (value > 500000000)
    {
        strcpy(str, ">500000000");
        return 10;
    }

    u16 len;

    // need to split in 2 conversions ?
    if (value > 10000)
    {
        const u16 v1 = value / (u16) 10000;
        const u16 v2 = value % (u16) 10000;

        len = uint16ToStr(v1, str, (minsize > 4)?(minsize - 4):1);
        len += uint16ToStr(v2, str + len, 4);
    }
    else len = uint16ToStr(value, str, minsize);

    return len;
}

static u16 uint16ToStr(u16 value, char *str, u16 minsize)
{
    u16 length;
    char *dst;
    u16 v;

    length = digits10(value);
    if (length < minsize) length = minsize;
    dst = &str[length];
    *dst = 0;
    v = value;

    while (v >= 100)
    {
        const u16 quot = v / 100;
        const u16 remain = v % 100;

        const u16 i = remain * 2;
        v = quot;

        *--dst = digits[i + 1];
        *--dst = digits[i + 0];
    }

    // handle last 1-2 digits
    if (v < 10) *--dst = '0' + v;
    else
    {
        const u16 i = v * 2;

        *--dst = digits[i + 1];
        *--dst = digits[i + 0];
    }

    // pad with '0'
    while(dst != str) *--dst = '0';

    return length;
}

void intToHex(u32 value, char *str, u16 minsize)
{
	char Output[16];
	int len = 0;

	if (value == 0)
	{
		Output[len++] = '0';
	}

	while (value)
	{
		int rem = value % 16;
		Output[len++] = (rem > 9) ? (rem - 10) + 'A' : rem + '0';
		value /= 16;
	}

	int pad = minsize > len ? minsize - len : 0;
	for (int k = 0; k < pad; ++k)
		*str++ = '0';
	
	for (int k = 0; k < len; ++k)
		*str++ = Output[k];

	*str = 0;
}

void fix32ToStr(fix32 value, char *str, u16 numdec)
{
    char *dst = str;
    fix32 v = value;

    if (v < 0)
    {
        v = -v;
        *dst++ = '-';
    }

    dst += uintToStr(fix32ToInt(v), dst, 1);
    *dst++ = '.';

    // get fractional part
    const u16 frac = (((u16) fix32Frac(v)) * (u16) 1000) / ((u16) 1 << FIX32_FRAC_BITS);
    u16 len = uint16ToStr(frac, dst, 1);

    if (len < numdec)
    {
        // need to add ending '0'
        dst += len;
        while(len++ < numdec) *dst++ = '0';
        // mark end here
        *dst = 0;
    }
    else dst[numdec] = 0;
}

void fix16ToStr(fix16 value, char *str, u16 numdec)
{
    char *dst = str;
    fix16 v = value;

    if (v < 0)
    {
        v = -v;
        *dst++ = '-';
    }

    dst += uint16ToStr(fix16ToInt(v), dst, 1);
    *dst++ = '.';

    // get fractional part
    const u16 frac = (((u16) fix16Frac(v)) * (u16) 1000) / ((u16) 1 << FIX16_FRAC_BITS);
    u16 len = uint16ToStr(frac, dst, 1);

    if (len < numdec)
    {
        // need to add ending '0'
        dst += len;
        while(len++ < numdec) *dst++ = '0';
        // mark end here
        *dst = 0;
    }
    else dst[numdec] = 0;
}


static u16 digits10(const u16 v)
{
    if (v < P02)
    {
        if (v < P01) return 1;
        return 2;
    }
    else
    {
        if (v < P03) return 3;
        if (v < P04) return 4;
        return 5;
    }
}

#if (ENABLE_NEWLIB == 0)
static u16 skip_atoi(const char **s)
{
    u16 i = 0;

    while(isdigit(**s))
        i = (i * 10) + *((*s)++) - '0';

    return i;
}

static u16 vsprintf(char *buf, const char *fmt, va_list args)
{
    char tmp_buffer[12];
    s16 i;
    s16 len;
    s16 *ip;
    u16 num;
    char *s;
    const char *hexchars;
    char *str;
    s16 left_align;
    s16 plus_sign;
    s16 zero_pad;
    s16 space_sign;
    s16 field_width;
    s16 precision;

    for (str = buf; *fmt; ++fmt)
    {
        if (*fmt != '%')
        {
            *str++ = *fmt;
            continue;
        }

        space_sign = zero_pad = plus_sign = left_align = 0;

        // Process the flag
repeat:
        ++fmt;          // this also skips first '%'

        switch (*fmt)
        {
        case '-':
            left_align = 1;
            goto repeat;

        case '+':
            plus_sign = 1;
            goto repeat;

        case ' ':
            if ( !plus_sign )
                space_sign = 1;

            goto repeat;

        case '0':
            zero_pad = 1;
            goto repeat;
        }

        // Process field width and precision

        field_width = precision = -1;

        if (isdigit(*fmt))
            field_width = skip_atoi(&fmt);
        else if (*fmt == '*')
        {
            ++fmt;
            // it's the next argument
            field_width = va_arg(args, s16);

            if (field_width < 0)
            {
                field_width = -field_width;
                left_align = 1;
            }
        }

        if (*fmt == '.')
        {
            ++fmt;

            if (isdigit(*fmt))
                precision = skip_atoi(&fmt);
            else if (*fmt == '*')
            {
                ++fmt;
                // it's the next argument
                precision = va_arg(args, s16);
            }

            if (precision < 0)
                precision = 0;
        }

        if ((*fmt == 'h') || (*fmt == 'l') || (*fmt == 'L'))
            ++fmt;

        if (left_align)
            zero_pad = 0;

        switch (*fmt)
        {
        case 'c':
            if (!left_align)
                while(--field_width > 0)
                    *str++ = ' ';

            *str++ = (unsigned char) va_arg(args, s16);

            while(--field_width > 0)
                *str++ = ' ';

            continue;

        case 's':
            s = va_arg(args, char *);

            if (!s)
                s = "<NULL>";

            len = strnlen(s, precision);

            if (!left_align)
                while(len < field_width--)
                    *str++ = ' ';

            for (i = 0; i < len; ++i)
                *str++ = *s++;

            while(len < field_width--)
                *str++ = ' ';

            continue;

        case 'p':
            if (field_width == -1)
            {
                field_width = 2 * sizeof(void *);
                zero_pad = 1;
            }

            hexchars = uppercase_hexchars;
            goto hexa_conv;

        case 'x':
            hexchars = lowercase_hexchars;
            goto hexa_conv;

        case 'X':
            hexchars = uppercase_hexchars;

hexa_conv:
            s = &tmp_buffer[12];
            *--s = 0;
            num = va_arg(args, u16);

            if (!num)
                *--s = '0';

            while(num)
            {
                *--s = hexchars[num & 0xF];
                num >>= 4;
            }

            num = plus_sign = 0;

            break;

        case 'n':
            ip = va_arg(args, s16*);
            *ip = (str - buf);
            continue;

        case 'u':
            s = &tmp_buffer[12];
            *--s = 0;
            num = va_arg(args, u16);

            if (!num)
                *--s = '0';

            while(num)
            {
                *--s = (num % 10) + 0x30;
                num /= 10;
            }

            num = plus_sign = 0;

            break;

        case 'd':
        case 'i':
            s = &tmp_buffer[12];
            *--s = 0;
            i = va_arg(args, s16);

            if (!i)
                *--s = '0';

            if (i < 0)
            {
                num = 1;

                while(i)
                {
                    *--s = 0x30 - (i % 10);
                    i /= 10;
                }
            }
            else
            {
                num = 0;

                while(i)
                {
                    *--s = (i % 10) + 0x30;
                    i /= 10;
                }
            }

            break;

        default:
            continue;
        }

        len = strnlen(s, precision);

        if (num)
        {
            *str++ = '-';
            field_width--;
        }
        else if (plus_sign)
        {
            *str++ = '+';
            field_width--;
        }
        else if (space_sign)
        {
            *str++ = ' ';
            field_width--;
        }

        if ( !left_align)
        {
            if (zero_pad)
            {
                while(len < field_width--)
                    *str++ = '0';
            }
            else
            {
                while(len < field_width--)
                    *str++ = ' ';
            }
        }

        for (i = 0; i < len; ++i)
            *str++ = *s++;

        while(len < field_width--)
            *str++ = ' ';
    }

    *str = '\0';

    return str - buf;
}

u16 sprintf(char *buffer, const char *fmt, ...)
{
    va_list args;
    u16 i;

    va_start(args, fmt);
    i = vsprintf(buffer, fmt, args);
    va_end(args);

    return i;
}

/*	$OpenBSD: strcasecmp.c,v 1.6 2005/08/08 08:05:37 espie Exp $	*/
/*
 * Copyright (c) 1987, 1993
 *	The Regents of the University of California.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

static const u8 charmap[] = {
	'\000', '\001', '\002', '\003', '\004', '\005', '\006', '\007',
	'\010', '\011', '\012', '\013', '\014', '\015', '\016', '\017',
	'\020', '\021', '\022', '\023', '\024', '\025', '\026', '\027',
	'\030', '\031', '\032', '\033', '\034', '\035', '\036', '\037',
	'\040', '\041', '\042', '\043', '\044', '\045', '\046', '\047',
	'\050', '\051', '\052', '\053', '\054', '\055', '\056', '\057',
	'\060', '\061', '\062', '\063', '\064', '\065', '\066', '\067',
	'\070', '\071', '\072', '\073', '\074', '\075', '\076', '\077',
	'\100', '\141', '\142', '\143', '\144', '\145', '\146', '\147',
	'\150', '\151', '\152', '\153', '\154', '\155', '\156', '\157',
	'\160', '\161', '\162', '\163', '\164', '\165', '\166', '\167',
	'\170', '\171', '\172', '\133', '\134', '\135', '\136', '\137',
	'\140', '\141', '\142', '\143', '\144', '\145', '\146', '\147',
	'\150', '\151', '\152', '\153', '\154', '\155', '\156', '\157',
	'\160', '\161', '\162', '\163', '\164', '\165', '\166', '\167',
	'\170', '\171', '\172', '\173', '\174', '\175', '\176', '\177',
	'\200', '\201', '\202', '\203', '\204', '\205', '\206', '\207',
	'\210', '\211', '\212', '\213', '\214', '\215', '\216', '\217',
	'\220', '\221', '\222', '\223', '\224', '\225', '\226', '\227',
	'\230', '\231', '\232', '\233', '\234', '\235', '\236', '\237',
	'\240', '\241', '\242', '\243', '\244', '\245', '\246', '\247',
	'\250', '\251', '\252', '\253', '\254', '\255', '\256', '\257',
	'\260', '\261', '\262', '\263', '\264', '\265', '\266', '\267',
	'\270', '\271', '\272', '\273', '\274', '\275', '\276', '\277',
	'\300', '\301', '\302', '\303', '\304', '\305', '\306', '\307',
	'\310', '\311', '\312', '\313', '\314', '\315', '\316', '\317',
	'\320', '\321', '\322', '\323', '\324', '\325', '\326', '\327',
	'\330', '\331', '\332', '\333', '\334', '\335', '\336', '\337',
	'\340', '\341', '\342', '\343', '\344', '\345', '\346', '\347',
	'\350', '\351', '\352', '\353', '\354', '\355', '\356', '\357',
	'\360', '\361', '\362', '\363', '\364', '\365', '\366', '\367',
	'\370', '\371', '\372', '\373', '\374', '\375', '\376', '\377',
};

s16 strcasecmp(const char *s1, const char *s2)
{
	const u8* cm = charmap;
	const u8* us1 = (const u8*)s1;
	const u8* us2 = (const u8*)s2;
	while (cm[*us1] == cm[*us2++])
		if (*us1++ == '\0')
			return 0;
	return (cm[*us1] - cm[*--us2]);
}

s16 strncasecmp(const char *s1, const char *s2, int n)
{
	if (n != 0)
	{
		const u8* cm = charmap;
		const u8* us1 = (const u8*)s1;
		const u8* us2 = (const u8*)s2;
		do
		{
			if (cm[*us1] != cm[*us2++])
				return (cm[*us1] - cm[*--us2]);
			if (*us1++ == '\0')
				break;
		} while (--n != 0);
	}
	return (0);
}
//--
#endif // ENABLE_NEWLIB
